import React, { useState, useEffect } from 'react';
//import ReactDOM from 'react-dom';  
//import { Container, Row, Col } from 'react-bootstrap';
import styles from '../styles/storepage.module.css';
import { Link } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import InputGroup from 'react-bootstrap/InputGroup';


const StorePage = () => {
    //   const URL = 'https://cs331e-379603.uc.r.appspot.com'
    let URL;
    if (window.location.hostname === 'localhost') {
        URL = 'http://127.0.0.1:5000/';
    } else {
        URL = 'https://cs331e-379603.uc.r.appspot.com';
    }


    const [data, setData] = useState(null);
    const [search, setSearch] = useState('');
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    // fetch data from backend
    useEffect(() => {
        fetch(`${URL}/stores`)
            .then(response => {
                if (response.ok) {
                    return response.json()
                }
                throw response;
            })
            .then(data => {
                setData(data);
            })
            .catch(error => {
                console.error("Error fetching data: ", error)
                setError(error);
            })
            .finally(() => {
                setLoading(false);
            })
    }, [])

    if (loading) return <div style={{ textAlign: 'center', fontSize: '20px' }}>Loading...</div>;
    if (error) return <div style={{ textAlign: 'center', fontSize: '20px' }}>Error fetching data...</div>;
    console.log(data)


    // Sorting
    const handleSort = (colIndex, reverse) => {
        if (!data) return;

        const sortedData = [...data].sort((a, b) => {
            const colA = a[colIndex];
            const colB = b[colIndex];

            if (typeof colA === "string" && typeof colB === "string") {
                return colA.localeCompare(colB);
            }
            else {
                return colA < colB ? -1 : 1;
            }
        });
        if (reverse === 1) sortedData.reverse();
        setData(sortedData);
    };

    // Set data in list format to dict w/ keys
    const newData = data.map((item) => ({
        id: item[0],
        name: item[1],
        rating: item[2],
        count: item[3],
        img: item[4],
        items: item[5],
        location: item[6]
    }));

    console.log(newData);

    return (

        <div>
            {/* <!-- Title --> */}
            <div className={`w3-content ${styles.bodyContent}`} style={{ maxWidth: '2000px', marginTop: '50px' }}>
                <h2 className="w3-text-black" style={{ textAlign: 'center', fontSize: '60px', fontWeight: "bold" }}>Stores</h2>
                <br />


                {/* <!-- Stores Section --> */}
                <div className="container overflow-hidden text-center">
                    <div>
                        <Button className="me-3" variant="outline-dark" onClick={() => handleSort(1, 0)}>Sort by name asc</Button>
                        <Button className="me-3" variant="outline-dark" onClick={() => handleSort(1, 1)}>Sort by name desc</Button>
                        <Button className="me-3" variant="outline-dark" onClick={() => handleSort(2, 0)}>Sort by rating asc</Button>
                        <Button className="me-3" variant="outline-dark" onClick={() => handleSort(2, 1)}>Sort by rating desc</Button>
                        <Form>
                            <InputGroup className='my-3'>

                                {/* onChange for search */}
                                <Form.Control
                                    onChange={(e) => setSearch(e.target.value)}
                                    placeholder='Search stores'
                                />
                            </InputGroup>
                        </Form>
                    </div>
                    <div className={`${styles.row} mx-auto text-center`}>

                        {newData
                            .filter((item) => {
                                return search.toLowerCase() === ''
                                    ? item
                                    : item.name.toLowerCase().includes(search.toLowerCase());
                            }).map((store) => {

                                return (
                                    <>
                                        {/* <!-- row 1 col 1 --> */}
                                        <div className="col" style={{ padding: 20 }} key={store.id}>
                                            <Link to={`/store/${store.id}`} state={{ store }} style={{ "textDecoration": "none", color: "black" }}>
                                                <div className={`card ${styles.card}`} style={{ width: '23rem' }}>
                                                    <img src={store.img} className="card-img-top" style={{ height: '200px' }} alt={store.name} />
                                                    <div className="card-body">
                                                        <h4 className="card-title title" style={{ textTransform: 'capitalize', fontWeight: 'bold' }}>{store.name}</h4>
                                                        <hr />
                                                        <p className="card-text content">Rating: {store.rating}</p>
                                                        <p className="card-text content">Store count: {store.count}</p>
                                                    </div>
                                                </div>
                                            </Link>
                                        </div>
                                        <br />
                                    </>

                                )
                            })
                        }
                    </div>
                </div>
            </div>

            <footer className={`text-center text-white ${styles.myFooter}`} style={{ backgroundColor: '#fafafa' }}>
                <div className="text-center text-dark p-3" style={{ backgroundColor: 'rgba(154, 152, 152, 0.2)' }}>
                    © 2023 Copyright: GroceryFinds
                </div>
            </footer>
        </div>
    );
};

export default StorePage;