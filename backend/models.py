from flask import Flask
from dataclasses import dataclass
from flask_sqlalchemy import SQLAlchemy
import os
from flask_cors import CORS


# Google Cloud SQL
USER = "postgres"
PASSWORD = "abc123"

PUBLIC_IP_ADDRESS = "localhost:5432"
DBNAME = "groceryfindsdb"

# Configuration
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get(
    "DB_STRING", f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')
# to suppress a warning message
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
app.app_context().push()


# ------------
# many-to-many relationship tables
# ------------
item_stores = db.Table('item_stores',
                       db.Column('item_id', db.Integer, db.ForeignKey(
                           'item.id'), primary_key=True),
                       db.Column('store_id', db.Integer, db.ForeignKey(
                           'store.id'), primary_key=True)
                       )

item_locations = db.Table('item_locations',
                          db.Column('item_id', db.Integer, db.ForeignKey(
                              'item.id'), primary_key=True),
                          db.Column('location_id', db.Integer, db.ForeignKey(
                              'location.id'), primary_key=True)
                          )


store_locations = db.Table('store_locations',
                           db.Column('store_id', db.Integer, db.ForeignKey(
                               'store.id'), primary_key=True),
                           db.Column('location_id', db.Integer, db.ForeignKey(
                               'location.id'), primary_key=True)
                           )


# ------------
# Item
# ------------
# @dataclass
class Item(db.Model):
    """
    Item class has 6 attributes 
    id
    name
    quantity
    image_url
    item_stores
    item_locations
    """
    __tablename__ = 'item'

    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String, nullable=False, unique=True)
    quantity = db.Column(db.String, nullable=False)
    image_url = db.Column(db.String, nullable=False)
    item_stores = db.relationship('Store', secondary=item_stores, lazy='subquery',
                                  backref=db.backref('items', lazy=True))
    item_locations = db.relationship('Location', secondary=item_locations, lazy='subquery',
                                     backref=db.backref('items', lazy=True))


# ------------
# Store
# ------------
# @dataclass
class Store(db.Model):
    """
    Store class has 6 attributes 
    id
    name
    rating (out of 5)
    store_count
    image_url
    item_stores (backreferenced)
    store_locations
    """
    __tablename__ = 'store'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, unique=True)
    rating = db.Column(db.Float, nullable=False)
    store_count = db.Column(db.Integer, nullable=False)
    image_url = db.Column(db.String, nullable=False)
    store_locations = db.relationship('Location', secondary=store_locations, lazy='subquery',
                                      backref=db.backref('stores', lazy=True))


# ------------
# Location
# ------------
# @dataclass
class Location(db.Model):
    """
    Location class has 8 attributes 
    id
    name
    capital
    image_url
    population
    region
    item_stores (backreferenced)
    store_locations (backreferenced)
    """
    __tablename__ = 'location'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, unique=True)
    capital = db.Column(db.String, nullable=False)
    image_url = db.Column(db.String, nullable=False)
    population = db.Column(db.Integer, nullable=False)
    region = db.Column(db.String, nullable=False)


db.drop_all()
db.create_all()
