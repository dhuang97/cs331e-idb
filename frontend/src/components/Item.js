import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { useNavigate, useLocation, Link } from 'react-router-dom';
import logoFull from '../assets/logoFull.png';
import styles from '../styles/Itemspage.module.css';
import eggs from '../assets/eggs.png';
import strawberries from '../assets/strawberries.png';
import milk from '../assets/milk.png';
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";

const ItemPage = () => {
	// const URL = 'https://cs331e-379603.uc.r.appspot.com'
	//const URL = 'http://10.165.194.65:5000'
	let URL;
	if (window.location.hostname === 'localhost') {
		URL = 'http://127.0.0.1:5000/';
	} else {
		URL = 'https://cs331e-379603.uc.r.appspot.com';
	}

	const [items, setItems] = useState(null);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(false);
	const [sortConfig, setSortConfig] = useState(true);
	const navigate = useNavigate();
	const location = useLocation();
	const query = new URLSearchParams(location.search).get('q');
	const [currentPage, setCurrentPage] = useState(0);
	const ITEMS_PER_PAGE = 25;
	const [search, setSearch] = useState('');


	useEffect(() => {
		async function fetchData() {
			try {
				const response = await axios.get(`${URL}/items`);
				setItems(response.data);
				setLoading(false);
			} catch (error) {
				console.error('Error fetching data:', error);
				setLoading(false);
				setError(true);
			}
		}

		fetchData();
	}, []);

	if (loading) {
		return <div style={{ textAlign: 'center', fontSize: '20px' }}>Loading...</div>;
	}

	if (error) {
		return <div>Error fetching data</div>;
	}
	const displayedItems = items.slice(
		currentPage * ITEMS_PER_PAGE,
		(currentPage + 1) * ITEMS_PER_PAGE
	);

	const handlePageClick = ({ selected }) => {
		setCurrentPage(selected);
	}

	const handleSort = (colIndex, reverse) => {
		if (!items) return;

		const sortedItems = [...items].sort((a, b) => {
			const colA = a[colIndex].toLowerCase();
			const colB = b[colIndex].toLowerCase();

			if (typeof colA === "string" && typeof colB === "string") {
				return colA.localeCompare(colB);
			}
			else {
				return typeof colA < typeof colB ? -1 : 1;
			}
		});
		if (reverse === 1) sortedItems.reverse();
		setItems(sortedItems);
	};
	const newItems = items.map((data) => ({
		id: data[0],
		name: data[1],
		quantity: data[2],
		img: data[3],
		stores: data[4],
		locations: data[5]
	}));
	console.log(newItems);
	return (
		<div>
			{/* Title */}
			<div className={`w3-content ${styles.bodyContent}`} style={{ maxWidth: '2000px', marginTop: '50px' }}>
				<h2 className="w3-text-black" style={{ textAlign: 'center', textShadow: '1px 1px 0 #8f8c8c', fontSize: '60px' }}>Items</h2>
				<center>
					<Button className="me-3" variant="outline-dark" onClick={() => handleSort(1, 0)}>Sort by name asc</Button>
					<Button className="me-3" variant="outline-dark" onClick={() => handleSort(1, 1)}>Sort by name desc</Button>
				</center>
				<br />


				<div className="container overflow-hidden text-center">
					<Form>
						<InputGroup className='my-3'>

							{/* onChange for search */}
							<Form.Control
								onChange={(e) => setSearch(e.target.value)}
								placeholder='Search items'
							/>
						</InputGroup>
					</Form>

					<div className={`${styles.row} mx-auto text-center`}>
						{newItems.filter((data) => {
							return search.toLowerCase() === ''
								? data
								: data.name.toLowerCase().includes(search.toLowerCase());
						}).map((item) => {

							return (
								<>
									<div className="col" style={{ padding: 20 }} key={item.id}>
										<Link to={`/item/${item.id}`} state={{ item }} style={{ "textDecoration": "none", color: "black" }}>
											<div className={`card ${styles.card}`} style={{ width: '23rem' }}>
												<img src={item.img} className="card-img-top" alt="item.img" style={{ "max-height": "170px" }} />
												<div className="card-body">
													<h4 className="card-title title" style={{ textTransform: 'capitalize', fontWeight: 'bold' }}>{item.name}</h4>
													<hr></hr>
													<p className="card-text content">Quantity: {item.quantity}</p>
												</div>
											</div>
										</Link>
									</div>
								</>
							)
						})
						}
					</div>
				</div>
			</div>
			<center>
				<ReactPaginate
					pageCount={Math.ceil(items.length / ITEMS_PER_PAGE)}
					marginPagesDisplayed={2}
					pageRangeDisplayed={5}
					onPageChange={handlePageClick}
					containerClassName={styles.pagination}
					activeClassName={styles.active}
					className={styles.ReactPaginate}
				/>
			</center>


			<footer className={`text-center text-white ${styles.myFooter}`} style={{ backgroundColor: '#fafafa' }}>
				<div className="text-center text-dark p-3" style={{ backgroundColor: 'rgba(154, 152, 152, 0.2)' }}>
					© 2023 Copyright: GroceryFinds
				</div>
			</footer>
		</div>

	);
};

export default ItemPage;
