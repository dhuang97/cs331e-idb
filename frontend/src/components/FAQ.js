import React from 'react';
import { Container, Accordion, Card, Button } from 'react-bootstrap';
import styles from '../styles/FAQ.module.css';

const FrequentlyAskedQuestions = () => {
  return (
    <div>
      <Container className={styles.bodyContent}>
        <h1 className="text-center">Frequently Asked Questions</h1>
        <br/>
        <div className="card">
          <div className="card-header w3-text-black" style={{textShadow:'1px 1px 0 #8f8c8c', fontSize: '20px'}}>
            Here are some frequently asked questions about Grocery Finds
          </div>
          <div className="card-body">
            <h5 className="card-title w3-text-black" style={{textShadow:'1px 1px 0 #8f8c8c', fontSize: '18px'}}>What is Grocery Finds?</h5>
            <p className="card-text">Grocery Finds is an online platform that helps users 
            discover and compare various grocery items, models, and store locations. 
            Our goal is to make grocery shopping more convenient and efficient for our users.</p>
          </div>

          <div className="card-body">
            <h5 className="card-title w3-text-black" style={{textShadow:'1px 1px 0 #8f8c8c', fontSize: '18px'}}>How do I search for a specific grocery item?</h5>
            <p className="card-text">To search for a specific grocery item, simply type the item name in 
            the search bar located at the top of our website and click on the "Search" button. You will 
            then be presented with a list of matching items, models, and store locations.</p>
          </div>

          <div className="card-body">
            <h5 className="card-title w3-text-black" style={{textShadow:'1px 1px 0 #8f8c8c', fontSize: '18px'}}>Can I compare different grocery items or stores?</h5>
            <p className="card-text">Yes, you can compare different grocery items or stores on our platform. 
            When you search for an item or store, you can click on each result to view detailed information 
            and make comparisons.</p>
          </div>

          <div className="card-body">
            <h5 className="card-title w3-text-black" style={{textShadow:'1px 1px 0 #8f8c8c', fontSize: '18px'}}>How can I get in touch with Grocery Finds?</h5>
            <p className="card-text"> If you have any questions, concerns, or suggestions, feel free to contact 
            us through the "About" page. Our team will be more than happy to assist you.</p>
          </div>

      </div>
      </Container>
      <footer className={`text-center text-white ${styles.myFooter}`} style={{ backgroundColor: '#fafafa' }}>
      <div className="text-center text-dark p-3" style={{ backgroundColor: 'rgba(154, 152, 152, 0.2)' }}>
        © 2023 Copyright: GroceryFinds
      </div>
      </footer>
    </div>

  );
};

export default FrequentlyAskedQuestions;