import React from 'react';
import ReactDOM from 'react-dom/client';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import RootLayout from './routes/rootLayout';
import HomePage from './components/Homepage';
import AboutPage from './components/About';
import ItemPage from './components/Item';
import LocationPage from './components/Location';
import StorePage from './components/store';
import ModelsPage from './components/Models';
import ModelAttributesPage from './components/ModelAttributes';
import StoreComponent from './components/StoreComponent';
import ItemComponent from './components/ItemComponent';
import LocationComponent from './components/LocationComponent';
import FrequentlyAskedQuestions from './components/FAQ';


const router = createBrowserRouter([
  {
    path: '/', element: <RootLayout />,
    children: [
      { path: '/', element: <HomePage /> },
      { path: '/about', element: <AboutPage /> },
      { path: '/item', element: <ItemPage /> },
      { path: '/location', element: <LocationPage /> },
      { path: '/store', element: <StorePage /> },
      { path: '/store/:storeId', element: <StoreComponent /> },
      { path: '/models', element: <ModelsPage /> },
      { path: '/faq', element: <FrequentlyAskedQuestions /> },
      { path: '/models/items', element: <ModelAttributesPage modelClicked={'items'} /> },
      { path: '/models/stores', element: <ModelAttributesPage modelClicked={'stores'} /> },
      { path: '/models/locations', element: <ModelAttributesPage modelClicked={'locations'} /> },
      { path: '/item/:itemId', element: <ItemComponent /> },
      { path: '/location/:locationId', element: <LocationComponent /> }


    ]
  }
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
