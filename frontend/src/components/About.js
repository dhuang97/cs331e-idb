import React, {useState, useEffect} from 'react';
import { Container, Row, Col, Modal } from 'react-bootstrap';
import axios from 'axios';
import logoFull from '../assets/logoFull.png';
import styles from '../styles/Aboutpage.module.css';
import shaojie from '../assets/shaojie.jpg';
import emma from '../assets/emma-2.jpg';
import riley from '../assets/riley.jpg';
import darren from '../assets/darren.png';
import yaashi from '../assets/yaashi-2.jpg';
import react from '../assets/react.png';
import flask from '../assets/flask.png';
import bootstrap from '../assets/bootstrap.png';
import gitlab from '../assets/gitlab.png';
import postgresql from '../assets/postgresql.png';
import postman from '../assets/postman.png';
import sqlalchemy from '../assets/sqla.png';
import vscode from '../assets/vscode.png';
import slack from '../assets/slack.png';
import gcp from '../assets/gcp.png';
// import unittest from '../assets/unittest.png';

const AboutPage = () => {
    let URL;
    if (window.location.hostname === 'localhost'){
        URL = 'http://127.0.0.1:5000/';
    }else{
        URL = 'https://cs331e-379603.uc.r.appspot.com';
    }
    // const URL = 'https://cs331e-379603.uc.r.appspot.com'
    const project_id = '44366803';
    const [contributors, setContributors] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const members1 = [{name:'Shaojie Hou', major: "Mathematics", role: "Home/About Page", image:shaojie, unitest: "N/A", linkedin: "https://www.linkedin.com/in/shaojie-hou/"}, 
                    {name:'Emma Louis', major:"Mechanical Engineering", role:"Item Page", image:emma, unitest: "3", linkedin: "https://www.linkedin.com/in/emma-louis-ba502921a/"}, 
                    {name:'Riley Sample', major:"Economics", role:"Store Page", image:riley, unitest: "N/A", linkedin: "https://www.linkedin.com/in/riley-sample-89162613a/"}]
    const members2 = [{name:'Darren Huang', major:"MIS", role:"Model Page", image:darren, unitest: "N/A", linkedin: "https://www.linkedin.com/in/darrenhhuang/"},
                    {name:'Yaashi Khatri', major:"Aerospace Engineering", role:"Location Page", image:yaashi, unitest: "N/A", linkedin: "https://www.linkedin.com/in/yaashi-khatri-135349169/"}];
                    
    const tools = [{ name: 'React.js', image: react },
                    { name: 'Flask', image: flask },
                    { name: 'Bootstrap', image: bootstrap },
                    { name: 'Gitlab', image: gitlab },
                    { name: 'PostgreSQL', image: postgresql },
                    { name: 'Postman', image: postman },
                    { name: 'SQLAlchemy', image: sqlalchemy },
                    { name: 'VSCode', image: vscode },
                    { name: 'Slack', image: slack },
                    { name: '', image: gcp}
                      ];

    const [showModal, setShowModal] = useState(false);
    const unitTestsImage = '/unittest.png';
    const handleUnitTestsButtonClick = () => {
    setShowModal(true);
    };
    const handleCloseModal = () => {
    setShowModal(false);
    };

    let totalCommits = 0;
    let totalIssues = 0;
                      

    useEffect(() => {
        async function fetchData() {
            try {
            const response = await axios.get(`${URL}/gitlab-data/${project_id}`);
            setContributors(response.data);
            setLoading(false);
            } catch (error) {
            console.error('Error fetching data:', error);
            setLoading(false);
            setError(true);
            }
        }
        
        fetchData();
        }, [project_id]);

    if (loading) {
        return <div style={{textAlign: 'center', fontSize: '20px'}}>Loading...</div>;
    }
    
    if (error) {
        return <div>Error fetching data</div>;
      }

    return (
    <div>
        {/* <!-- Page content --> */}
        <div className={`w3-content ${styles.bodyContent}`} style={{maxWidth:'2000px', marginTop:'50px'}}>
            <h1 className="w3-text-black" style={{textAlign:'center', textShadow:'1px 1px 0 #8f8c8c', fontSize: '60px'}}>About Us</h1>
            <br/>
            <div className="w-75 mx-auto fs-3" style={{textAlign: 'center'}}>
            <p style={{display: 'inline-block', textAlign: 'left', fontWeight: 'bold'}}>
            GroceryFinds was created to empower consumers' purchasing decisions and to hold grocery stores 
            accountable for potential price gouging. Due to recent concerns over rising grocery prices, 
            price gouging by retailers has become an important issue and we want to level the playing field 
            for everyday people by giving them access to the pricing decisions being made by the stores that 
            can directly affect the market.
            </p>
            </div>
            <br/>

        {/* <!-- The About Us Section --> */}
            <div className="container overflow-hidden text-center">
                <div className={`${styles.row} mx-auto text-center`}>
                    {/* <!-- row 1 --> */}
                    {members1.map((member) => {
                    const commitCount = contributors[member.name]?.commit_count || 0;
                    const issueCount = contributors[member.name]?.issue_count || 0;
                    totalCommits += commitCount;
                    totalIssues += issueCount;
                    return (
                    <div className="col">
                        <div className={`card ${styles.card}`} style={{width: '23rem'}}>
                            <img src={member.image} className="card-img-top" alt="shaojie"/>
                            <div className="card-body">
                                <h5 className="card-title profileTitle">{member.name}</h5>
                                <p className="card-text profileContent">{member.major} Major</p>
                                <p className="card-text role">{member.role}</p>
                                <ul className="list-group">
                                <li className="list-group-item d-flex justify-content-between align-items-center">
                                    No. of commits
                                    <span className="badge bg-dark text-light">{commitCount}</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-center">
                                    No. of issues
                                    <span className="badge bg-dark text-light">{issueCount}</span>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-center">
                                    No. of unit tests
                                    <span className="badge bg-dark text-light">{member.unitest}</span>
                                </li>
                                </ul>
                                <a href={member.linkedin} className="stretched-link"></a>
                            </div>
                        </div>
                    </div>
                    );
                    })}

                </div>
                <br/>

                {/* <!-- row 2 --> */}
                <div className="row mx-auto text-center">
                {members2.map((member) => {
                    const commitCount = contributors[member.name]?.commit_count || 0;
                    const issueCount = contributors[member.name]?.issue_count || 0;
                    totalCommits += commitCount;
                    totalIssues += issueCount;
                    return (
                    <div className="col">
                        <div className={`card ${styles.card}`} style={{width: '23rem'}}>
                            <img src={member.image} className="card-img-top" alt="darren"/>
                            <div className="card-body">
                            <h5 className="card-title profileTitle">{member.name}</h5>
                            <p className="card-text profileContent">{member.major} Major</p>
                            <p className="card-text role">{member.role}</p>
                            <ul className="list-group">
                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                No. of commits
                                <span className="badge bg-dark text-light">{commitCount}</span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                No. of issues
                                <span className="badge bg-dark text-light">{issueCount}</span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                No. of unit tests
                                <span className="badge bg-dark text-light">{member.unitest}</span>
                            </li>
                            </ul>
                            <a href={member.linkedin} className="stretched-link"></a>
                            </div>
                        </div>
                    </div>
                    );
                    })}


                    <div className="col">
                        <div className={`card ${styles.card}`} style={{width: '23rem'}}>
                            <div className="card-body">


                                <br/>
                                <ul className="list-group">
                                    <li className={`list-group-item d-flex justify-content-between align-items-center ${styles.commit}`}>
                                    Total No. of commits
                                    <span className="badge bg-dark text-light">{totalCommits}</span>
                                    </li>
                                    <li className={`list-group-item d-flex justify-content-between align-items-center ${styles.commit}`}>
                                    Total No. of issues
                                    <span className="badge bg-dark text-light">{totalIssues}</span>
                                    </li>
                                    <li className={`list-group-item d-flex justify-content-between align-items-center ${styles.commit}`}>
                                    Total No. of unit tests
                                    <span className="badge bg-dark text-light">3</span>
                                    </li>
                                </ul>
                                <a href="https://gitlab.com/" className="stretched-link"></a>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <br/>
                <br/>

                <h2 className="w3-text-black" style={{textAlign: 'center', textShadow: '1px 1px 0 #8f8c8c', fontSize: '40px'}}>Gitlab Repository</h2>
                <br />
                <div className="btn-group-vertical btn-group-lg" role="group" aria-label="Vertical button group">
                    <button
                        type="button"
                        className="btn btn-outline-dark"
                        style={{width: '1100px', height: '70px', fontSize: '25px'}}
                        onClick={() => window.location.href='https://gitlab.com/dhuang97/cs331e-idb/-/issues'}>
                        GitLab Issue Tracker
                    </button>
                    <button
                        type="button"
                        className="btn btn-outline-dark"
                        style={{width: '1100px', height: '70px', fontSize: '25px'}}
                        onClick={() => window.location.href='https://gitlab.com/dhuang97/cs331e-idb'}>
                        GitLab Repo
                    </button>
                    <button
                        type="button"
                        className="btn btn-outline-dark"
                        style={{width: '1100px', height: '70px', fontSize: '25px'}}
                        onClick={() => window.location.href='https://gitlab.com/dhuang97/cs331e-idb/-/wikis/home'}>
                        GitLab Wiki
                    </button>
                </div>

                <br/>
                <br/>    
                <h2 className="w3-text-black" style={{textAlign: 'center', textShadow: '1px 1px 0 #8f8c8c', fontSize: '40px'}}>Tools We Used</h2>
                {/* Tools We Used Section */}
                <br/>    
                <Container>
                <Row className="justify-content-md-center">
                    {tools.map((tool, index) => (
                    <Col key={index} md={4} className="mb-4">
                        <img
                        src={tool.image}
                        alt={tool.name}
                        style={{ maxWidth: '150px', maxHeight: '150px' }}
                        />
                        <p>{tool.name}</p>
                    </Col>
                    ))}
                </Row>
                </Container>
                <br/>
                <br/>
                <h2 className="w3-text-black" style={{textAlign: 'center', textShadow: '1px 1px 0 #8f8c8c', fontSize: '40px'}}>Data Sources We Used</h2>
                <br />
                <div className="btn-group-vertical btn-group-lg" role="group" aria-label="Vertical button group">
                    <button
                        type="button"
                        className="btn btn-outline-dark"
                        style={{width: '1100px', height: '70px', fontSize: '25px'}}
                        onClick={() => window.location.href='https://restcountries.com/v3.1/all?fields=name,capital,flags,population,region'}>
                        Location API
                    </button>
                    <button
                        type="button"
                        className="btn btn-outline-dark"
                        style={{width: '1100px', height: '70px', fontSize: '25px'}}
                        onClick={() => window.location.href='https://us.openfoodfacts.org/api/v2/search?page_size=300&fields=product_name,quantity,image_url,stores,countries_tags'}>
                        Items API
                    </button>
                    <button
                        type="button"
                        className="btn btn-outline-dark"
                        style={{width: '1100px', height: '70px', fontSize: '25px'}}
                        onClick={() => window.location.href='https://gitlab.com/api/v4/projects/44366803/repository/commits'}>
                        GitLab Commits API
                    </button>
                    <button
                        type="button"
                        className="btn btn-outline-dark"
                        style={{width: '1100px', height: '70px', fontSize: '25px'}}
                        onClick={() => window.location.href='https://gitlab.com/api/v4/projects/44366803/issues'}>
                        GitLab Issues API
                    </button>
                </div>

                <br/>
                <br/>
                <br/>
                <h2 className="w3-text-black" style={{textAlign: 'center', textShadow: '1px 1px 0 #8f8c8c', fontSize: '40px'}}>Additional Information</h2>
                <br />
                <div className="btn-group-vertical btn-group-lg" role="group" aria-label="Vertical button group">
                    <button
                        type="button"
                        className="btn btn-outline-dark"
                        style={{width: '1100px', height: '70px', fontSize: '25px'}}
                        onClick={() => window.location.href='https://api.postman.com/collections/26466765-8f228df0-b303-4a95-8073-dd199de7c641?access_key=PMAT-01GXCB2X9Q6ZGGNMV4RNEDWFP3'}>
                        Postman Collection
                    </button>

                    <button
                        type="button"
                        className="btn btn-outline-dark"
                        style={{width: '1100px', height: '70px', fontSize: '25px'}}
                        onClick={() => window.location.href='https://speakerdeck.com/shaojiehou/groceryfinds-30017e94-3b90-4f9d-a18d-707b74c266e2'}>
                        Speaker Deck
                    </button>

                    <button
                        type="button"
                        className="btn btn-outline-dark"
                        style={{width: '1100px', height: '70px', fontSize: '25px'}}
                        // onClick={() =>(window.location.href='../assets/unittest.png')}
                        onClick={handleUnitTestsButtonClick}>
                        Unit Tests
                    </button>

                          {/* Modal */}
                    <Modal show={showModal} onHide={handleCloseModal} size="lg" centered>
                        <Modal.Header closeButton>
                        <Modal.Title>Unit Tests</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                        <img src={unitTestsImage} alt="Unit Tests" style={{ width: '100%', height: 'auto' }} />
                        </Modal.Body>
                    </Modal>
                </div>

            </div>
        </div>


      <footer className={`text-center text-white ${styles.myFooter}`} style={{ backgroundColor: '#fafafa' }}>
        <div className="text-center text-dark p-3" style={{ backgroundColor: 'rgba(154, 152, 152, 0.2)' }}>
          © 2023 Copyright: GroceryFinds
        </div>
      </footer>
    </div>
  );
};

export default AboutPage;
