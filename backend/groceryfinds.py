from flask import Flask, jsonify
from get_git_data import get_gitlab_data
from flask_cors import CORS, cross_origin
from create_db import app, db, Item, Store, Location, item_stores, item_locations, store_locations, load_json
import json
from modeldata import modeldata

CORS(app, resources={r"/*": {"origins": "*"}})

PROJECT_ID = '44366803'


@app.route('/profile')
def my_profile():
    response_body = {
        "name": "Nagato",
        "about": "Hello! I'm a full stack developer that loves python and javascript"
    }
    return response_body


@app.route('/gitlab-data/<int:PROJECT_ID>')
def gitlab_data(PROJECT_ID):
    GITLAB_ACCESS_TOKEN = 'glpat-jxssdQGRoW6aVCSjPbB4'
    PROJECT_ID = '44366803'
    data = get_gitlab_data(PROJECT_ID, GITLAB_ACCESS_TOKEN)
    return jsonify(data)


@app.route('/test-gitlab-data/<int:PROJECT_ID>')
def test_gitlab_data(PROJECT_ID):
    access_token = 'glpat-jxssdQGRoW6aVCSjPbB4'
    data = get_gitlab_data(PROJECT_ID, access_token)
    return jsonify(data)


@app.route('/items', methods=['GET'])
@cross_origin(origin='*')
def items():
    result = modeldata('item', 'item_stores', 'item_locations')
    # app.logger.info(result)
    return jsonify(result[0])


@app.route('/items_columns', methods=['GET'])
def items_columns():
    result = modeldata('item', 'item_stores', 'item_locations')
    result = result[1]
    result = [res.replace('item_', '') for res in result]
    return jsonify(result)


@app.route('/stores', methods=['GET'])
def stores():
    result = modeldata('store', 'item_stores', 'store_locations')
    # app.logger.info(result)
    return jsonify(result[0])


@app.route('/stores_columns', methods=['GET'])
def stores_columns():
    result = modeldata('store', 'item_stores', 'store_locations')
    # app.logger.info(result)
    result = result[1]
    result = [res.replace('_store', '') if '_store' in res else res.replace(
        'store_', '') for res in result]
    return jsonify(result)


@app.route('/locations', methods=['GET'])
def locations():
    result = modeldata('location', 'item_locations', 'store_locations')
    # app.logger.info(result)
    return jsonify(result[0])


@app.route('/locations_columns', methods=['GET'])
def locations_columns():
    result = modeldata('location', 'item_locations', 'store_locations')
    # app.logger.info(result)
    result = result[1]
    result = [res.replace('_location', '') for res in result]
    return jsonify(result)


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
