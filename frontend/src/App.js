// export default App;
import { useState } from 'react'
import axios from "axios";
import logo from './logo.svg';
import './App.css';
import NavBar from './components/Navbar';
import HomePage from './components/Homepage';

function App() {

   // new line start
  const [profileData, setProfileData] = useState(null)

  function getData() {
    axios({
      method: "GET",
      url:"/profile",
    })
    .then((response) => {
      const res =response.data
      setProfileData(({
        profile_name: res.name,
        about_me: res.about}))
    }).catch((error) => {
      if (error.response) {
        console.log(error.response)
        console.log(error.response.status)
        console.log(error.response.headers)
        }
    })}
    //end of new line 

  return (
    <div className="App">
      <NavBar />
      <HomePage />
    </div>
  );
}

export default App;