import requests
import json

item_api_url = "https://us.openfoodfacts.org/api/v2/search?page_size=1000&fields=product_name,quantity,image_url,stores,countries_tags"
store_api_url = ""
location_api_url = "https://restcountries.com/v3.1/all?fields=name,capital,flags,population,region"


def load_item_api_data(url):

    response = requests.request("GET", url)
    raw_data = json.loads(response.text)
    raw_items = raw_data['products']
    cleaned_data = []
    unique_item_names = set()

    i = 0
    for raw_item in raw_items:
        keys = raw_item.keys()
        # values = raw_item.values()

        # get rid of any items with non-english (non-ascii) characters
        # if len([value for value in values if type(value) == str and not value.isascii()]) > 0:
        #     continue
        if not str(raw_item).isascii():
            continue

        # clean up data and store in dictionary
        if 'product_name' in keys and 'quantity' in keys and 'stores' in keys and 'countries_tags' in keys and 'image_url' in keys:
            if raw_item['product_name'] and raw_item['quantity'] and raw_item['stores'] and raw_item['countries_tags'] and raw_item['image_url']:
                if raw_item['product_name'].lower() not in unique_item_names:
                    unique_item_names.add(raw_item['product_name'].lower())
                    cleaned_item = {}
                    cleaned_item['id'] = i
                    cleaned_item['name'] = raw_item['product_name'].lower()
                    cleaned_item['quantity'] = raw_item['quantity']
                    cleaned_item['image_url'] = raw_item['image_url']
                    raw_item_stores = raw_item['stores'].split(',')
                    cleaned_item['item_stores'] = [raw_store.lower()
                                                   for raw_store in raw_item_stores]
                    cleaned_item['item_locations'] = []
                    # format item_locations to be consistent with locations_api.json
                    for raw_country in raw_item['countries_tags']:
                        cleaned_country = raw_country[3:]
                        cleaned_country = cleaned_country.replace("-", " ")
                        cleaned_item['item_locations'].append(cleaned_country)

                    cleaned_data.append(cleaned_item)
                    i += 1

    final_item_data = {"Items": cleaned_data}

    with open("items_api.json", "w") as outfile:
        outfile.write(json.dumps(final_item_data))


def load_location_api_data(url):

    response = requests.request("GET", url)
    raw_locations = json.loads(response.text)
    cleaned_data = []
    unique_location_names = set()

    i = 0
    for raw_location in raw_locations:
        keys = raw_location.keys()

        # get rid of any items with non-english (non-ascii) characters
        if not str(raw_location).isascii():
            continue

        # clean up data and store in dictionary
        if 'flags' in keys and 'name' in keys and 'capital' in keys and 'region' in keys and 'population' in keys:
            if raw_location['flags'] and raw_location['name'] and raw_location['capital'] and raw_location['region'] and raw_location['population']:
                if raw_location['name']['common'] not in unique_location_names and raw_location['name']['common'].isascii():
                    unique_location_names.add(raw_location['name']['common'])
                    cleaned_location = {}
                    cleaned_location['id'] = i
                    cleaned_location['name'] = raw_location['name']['common'].lower(
                    )
                    cleaned_location['capital'] = raw_location['capital'][0]
                    cleaned_location['image_url'] = raw_location['flags']['png']
                    cleaned_location['population'] = raw_location['population']
                    cleaned_location['region'] = raw_location['region']
                    cleaned_data.append(cleaned_location)
                    i += 1

    final_location_data = {"Locations": cleaned_data}

    with open("locations_api.json", "w") as outfile:
        outfile.write(json.dumps(final_location_data))


load_item_api_data(item_api_url)
load_location_api_data(location_api_url)
