import React from 'react';
import { Navbar, Nav, Container, Form, FormControl, Button } from 'react-bootstrap';

const NavBar = () => {
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="/">Grocery Finds</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/" className="me-3">
              Home
            </Nav.Link>
            <Nav.Link href="/models" className="me-3">
              Model
            </Nav.Link>
            <Nav.Link href="/item" className="me-3">
              Item
            </Nav.Link>
            <Nav.Link href="/store" className="me-3">
              Store
            </Nav.Link>
            <Nav.Link href="/location" className="me-3">
              Location
            </Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
            
          </Nav>

          <Nav className='ms-auto d-inline-flex'>
            <Nav.Link href='/faq' style={{ fontFamily: 'unset', fontSize: '16px', color:'black' }}>
              FAQ
            </Nav.Link>
          </Nav>

        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NavBar;
