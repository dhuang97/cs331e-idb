import os
import sys
import unittest
from models import db, Item, Store, Location

# -----------
# DBTestCases
# -----------


class TestCreateDB(unittest.TestCase):
    # ---------
    # insertion
    # ---------

    def test_item_insert_1(self):
        s = Item(id='1000', name='Twix', quantity='10',
                 image_url='', item_stores=[], item_locations=[])
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Item).filter_by(id='1000').one()
        self.assertEqual(str(r.id), '1000')
        self.assertEqual(str(r.name), 'Twix')
        self.assertEqual(str(r.quantity), '10')

        db.session.query(Item).filter_by(id='1000').delete()
        db.session.commit()

    def test_store_insert_2(self):
        s = Store(id='500', name='Central Market', rating='5.0',
                  store_count='3', image_url='', store_locations=[])
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Store).filter_by(id='500').one()
        self.assertEqual(str(r.id), '500')
        self.assertEqual(str(r.name), 'Central Market')
        self.assertEqual(str(r.rating), '5.0')

        db.session.query(Store).filter_by(id='500').delete()
        db.session.commit()

    def test_location_insert_3(self):
        s = Location(id='1000', name='Germany', capital='Berlin',
                     image_url='', population='8320000', region='Europe')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Location).filter_by(id='1000').one()
        self.assertEqual(str(r.id), '1000')
        self.assertEqual(str(r.name), 'Germany')
        self.assertEqual(str(r.capital), 'Berlin')

        db.session.query(Location).filter_by(id='1000').delete()
        db.session.commit()


if __name__ == '__main__':
    unittest.main()
# end of code
