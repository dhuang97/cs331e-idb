import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { Form, InputGroup } from 'react-bootstrap';
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import styles from '../styles/ModelAttributesPage.module.css';


const ModelAttributesPage = ({ modelClicked }) => {

    const history = useNavigate();
    const [search, setSearch] = useState('')
    const [objects, setObjects] = useState([]);
    const [objColumns, setObjColumns] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [currentPage, setCurrentPage] = useState(0);
    const [sortColumn, setSortColumn] = useState(null);
    const [sortOrder, setSortOrder] = useState('ascending');

    const ITEMS_PER_PAGE = 25;
    const displayedObjects = objects.slice(
        currentPage * ITEMS_PER_PAGE,
        (currentPage + 1) * ITEMS_PER_PAGE
    );

    function detectHostname() {
        let URL;
        if (window.location.hostname === "localhost" || window.location.hostname === "127.0.0.1") {
            URL = 'http://localhost:5000';
        } else {
            URL = 'https://cs331e-379603.uc.r.appspot.com';
        }
        return URL;
    }

    function isList(obj) {
        return (
            Array.isArray(obj)
        );
    };

    const handlePageClick = ({ selected }) => {
        setCurrentPage(selected);
    }

    function isImage(obj) {
        if (typeof obj === 'string') {
            return (
                obj.startsWith("https://") || obj.startsWith("http://")
            );
        } else {
            return false;
        }
    }

    function serializeObject(item) {
        let newItem = {};
        if (modelClicked === 'items') {
            newItem = {
                id: item[0],
                name: item[1],
                quantity: item[2],
                img: item[3],
                stores: item[4],
                locations: item[5]
            }
        } else if (modelClicked === 'stores') {
            newItem = {
                id: item[0],
                name: item[1],
                rating: item[2],
                count: item[3],
                img: item[4],
                items: item[5],
                location: item[6]
            }
        } else {
            newItem = {
                id: item[0],
                name: item[1],
                capital: item[2],
                img: item[3],
                population: item[4],
                region: item[5],
                items: item[6],
                stores: item[7]
            }
        }
        return newItem;
    }

    const handleSort = (column) => {
        if (sortColumn === column) {
            setSortOrder(sortOrder === 'ascending' ? 'descending' : 'ascending');
        } else {
            setSortColumn(column);
            setSortOrder('ascending');
        }

        const sortedObjects = [...objects].sort((a, b) => {
            const sortMultiplier = sortOrder === 'ascending' ? 1 : -1;
            if (a[sortColumn] < b[sortColumn]) {
                return -1 * sortMultiplier;
            }
            if (a[sortColumn] > b[sortColumn]) {
                return 1 * sortMultiplier;
            }
            return 0;
        });

        setObjects(sortedObjects);
    };

    function handleRowClick(object) {
        const link = `/${modelClicked.slice(0, -1)}/${object[0]}`;
        const stateValue = serializeObject(object);
        if (modelClicked === 'items') {
            history(link, { state: { item: stateValue } });
        } else if (modelClicked === 'stores') {
            history(link, { state: { store: stateValue } });
        } else {
            history(link, { state: { location: stateValue } });
        }
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const URL = detectHostname();
                const objResponse = await axios.get(`${URL}/${modelClicked}`);
                const objColResponse = await axios.get(`${URL}/${modelClicked}_columns`);
                setObjects(objResponse.data);
                setObjColumns(objColResponse.data);
                setLoading(false);
            } catch (error) {
                console.error('Error fetching data:', error);
                setLoading(false);
                setError(true);
            }
        }

        fetchData();
    }, [modelClicked]);

    useEffect(() => {
        if (sortColumn !== null) {
            const sortedObjects = [...objects].sort((a, b) => {
                const sortMultiplier = sortOrder === 'ascending' ? 1 : -1;
                if (a[sortColumn] < b[sortColumn]) {
                    return -1 * sortMultiplier;
                }
                if (a[sortColumn] > b[sortColumn]) {
                    return 1 * sortMultiplier;
                }
                return 0;
            });
            setObjects(sortedObjects);
        }
    }, [sortColumn, sortOrder]);

    if (loading) {
        return <div style={{ textAlign: 'center', fontSize: '20px' }}>Loading...</div>;
    }

    if (error) {
        return <div>Error fetching data</div>;
    }

    return (
        <div>
            <div
                id="page-container"
                className="w3-content"
                style={{
                    maxWidth: "2000px", marginTop: "46px"
                }}
            >
                <h1 className="w3-text-black" style={{ "text-align": "center", "text-transform": "uppercase" }}>{modelClicked} Model</h1>
                <hr className="center" />
                <br />

                <Form className='search-bar'>
                    <InputGroup className='my-3'>

                        {/* onChange for search */}
                        <Form.Control
                            onChange={(e) => setSearch(e.target.value)}
                            placeholder={`Search ${modelClicked} by name`}
                        />
                    </InputGroup>
                </Form>

                <table className="table table-striped table-bordered">
                    <thead className="table-dark">
                        <tr>
                            {objColumns.map((objColumn, idx) => (
                                <th scope="col">
                                    <button type="button" onClick={() => handleSort(idx)}>
                                        {(idx === sortColumn) ? (
                                            (sortOrder === 'ascending') ? (
                                                <span><strong>{objColumn} </strong>👇</span>
                                            ) : (
                                                <span><strong>{objColumn} </strong>☝️</span>
                                            )
                                        ) : (
                                            <span><strong>{objColumn}</strong></span>
                                        )}
                                        <span style={{ opacity: "0.5", fontSize: "12px", marginLeft: "1em" }}><i>(click to sort)</i></span>
                                    </button>
                                </th>
                            ))}
                        </tr>
                    </thead>
                    <tbody>
                        {search.toLowerCase() === '' ? (
                            displayedObjects.map((object) => (
                                <tr onClick={() => { handleRowClick(object) }}>
                                    {object.map((attr) => (
                                        isList(attr) ? (
                                            <td style={{ maxWidth: "20%" }}>
                                                <ul>
                                                    {attr.map((elem) => (
                                                        <li>{elem}</li>
                                                    ))}
                                                </ul>
                                            </td>
                                        ) : (
                                            isImage(attr) ? (
                                                <img src={attr} alt={attr} style={{ maxHeight: "150px" }} />
                                            ) : (
                                                <td style={{ maxWidth: "20%" }}>{attr}</td>
                                            )
                                        )
                                    ))}
                                </tr>
                            ))) : (
                            objects.filter(obj => obj[1].includes(search.toLowerCase())).map((object) => (
                                <tr onClick={() => { handleRowClick(object) }}>
                                    {object.map((attr) => (
                                        isList(attr) ? (
                                            <td style={{ maxWidth: "20%" }}>
                                                <ul>
                                                    {attr.map((elem) => (
                                                        <li>{elem}</li>
                                                    ))}
                                                </ul>
                                            </td>
                                        ) : (
                                            isImage(attr) ? (
                                                <img src={attr} alt={attr} style={{ maxHeight: "150px" }} />
                                            ) : (
                                                <td style={{ maxWidth: "20%" }}>{attr}</td>
                                            )
                                        )
                                    ))}
                                </tr>
                            ))
                        )
                        }
                    </tbody>
                </table>

                <center>
                    <ReactPaginate
                        pageCount={Math.ceil(objects.length / ITEMS_PER_PAGE)}
                        pageRangeDisplayed={15}
                        onPageChange={handlePageClick}
                        containerClassName={styles.pagination}
                        activeClassName={styles.active}
                        className={styles.ReactPaginate}
                    />
                </center>

                <center>
                    <Link to='/models' className="btn btn-primary btn-lg button center" style={{ marginTop: "25px", marginBottom: "100px" }}>
                        Back to Models
                    </Link>
                </center>

            </div >

            <footer className={`text-center text-white ${styles.myFooter}`} style={{ backgroundColor: '#fafafa' }}>
                <div className="text-center text-dark p-3" style={{ backgroundColor: 'rgba(154, 152, 152, 0.2)' }}>
                    © 2023 Copyright: GroceryFinds
                </div>
            </footer>

        </div>
    );
};

export default ModelAttributesPage;