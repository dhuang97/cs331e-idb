import React, { useState, useEffect } from 'react';
import styles from '../styles/LocationPages.module.css';
import { Link, useLocation } from 'react-router-dom';
import { InputGroup, Form } from 'react-bootstrap';
import axios from 'axios';

const LocationComponent = () => {
	// const URL = 'https://cs331e-379603.uc.r.appspot.com'
	const history = useLocation();
	const location = history.state.location;
	const [items, setItems] = useState(null);
	const [stores, setStores] = useState(null);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(false);
	const [search, setSearch] = useState('');

	let URL;
	if (window.location.hostname === 'localhost') {
		URL = 'http://127.0.0.1:5000/';
	} else {
		URL = 'https://cs331e-379603.uc.r.appspot.com';
	}

	useEffect(() => {
		async function fetchData() {
			try {
				const itemResponse = await axios.get(`${URL}/items`);
				const storeResponse = await axios.get(`${URL}/stores`);
				setItems(itemResponse.data);
				setStores(storeResponse.data);
				setLoading(false);
			} catch (error) {
				console.error('Error fetching data:', error);
				setLoading(false);
				setError(true);
			}
		}
		fetchData();
	}, []);

	if (loading) { return <div style={{ textAlign: 'center', fontSize: '20px' }}>Loading...</div>; }
	if (error) { return <div style={{ textAlign: 'center', fontSize: '20px' }}>Error fetching data...</div>; }

	const newItems = items.map((item) => ({
		id: item[0],
		name: item[1],
		quantity: item[2],
		img: item[3],
		stores: item[4],
		locations: item[5]
	}));

	const newStores = stores.map((store) => ({
		id: store[0],
		name: store[1],
		rating: store[2],
		count: store[3],
		img: store[4],
		items: store[5],
		location: store[6]
	}))

	const filteredItems = newItems.filter(item => location.items.includes(item.name));
	const filteredStores = newStores.filter(store => location.stores.includes(store.name));

	function capFirstLetter(string) {
		const words = string.split(' ');
		for (let i = 0; i < words.length; i++) {
			words[i] = words[i][0].toUpperCase() + words[i].substr(1);
		}
		return words.join(' ');
	}

	return (
		<div>
			{/* <!-- Title --> */}
			<div className={`w3-content ${styles.bodyContent}`} style={{ maxWidth: '2000px', marginTop: '50px' }}>
				<h1 className="w3-text-black" style={{
					textAlign: 'center', fontSize: '60px', textTransform: 'capitalize',
					fontWeight: "bold"
				}}>{location.name}</h1>
				<br />

				{/* <!-- Stores Section --> */}
				<div className="container overflow-hidden">
					<div>
						<Form>
							<InputGroup className='my-3'>

								{/* onChange for search */}
								<Form.Control
									onChange={(e) => setSearch(e.target.value)}
									placeholder={`Search items and stores within ${capFirstLetter(location.name)}`}
								/>
							</InputGroup>
						</Form>
					</div>

					<div className={`${styles.row} mx-auto text-center`}>
						<div className="col" style={{ padding: 20 }}>
							<div className={`card text-center`} style={{ width: '23rem', height: '26rem' }}>
								<img src={location.img} className="card-img-top" style={{ height: '200px' }} alt={location.name} />
								<div className="card-body">
									<h4 className="card-title title" style={{ textTransform: 'capitalize', fontWeight: 'bold' }}>{location.name}</h4>
									<hr />
									<p className="card-text content">Capital: {location.capital}</p>
									<p className="card-text content">Population: {location.population}</p>
									<p className="card-text content">Region: {location.region}</p>
								</div>
							</div>
						</div>

						<div className="col" style={{ padding: 20 }}>
							<div className={`card text-center`} style={{ width: '23rem', height: '26rem' }}>
								<div className="card-body" style={{ overflowY: 'scroll' }}>
									<h4 className="card-title title" style={{ textTransform: 'capitalize', fontWeight: 'bold' }}>Items available:</h4>
									<hr></hr>
									{
										filteredItems && filteredItems
											.filter((item) => {
												return search.toLowerCase() === ''
													? item.name
													: item.name.toLowerCase().includes(search.toLowerCase());
											}).map((item) => {
												return (
													<>
														<ul className="list-group" style={{ listStyleType: 'none', textTransform: 'capitalize' }}>
															<li>
																<Link to={`/item/${item.id}`} state={{ item }} style={{ textDecoration: "none", color: "black" }}>
																	<div className={`card ${styles.card}`} style={{ padding: 5 }}>
																		<p className="card-text content">{item.name}</p>
																	</div>
																</Link>
															</li>
														</ul>
													</>
												)
											})
									}
								</div>
							</div>
						</div>

						<div className="col" style={{ padding: 20 }}>
							<div className={`card text-center`} style={{ width: '23rem', height: '26rem' }}>
								<div className="card-body" style={{ overflowY: 'scroll' }}>
									<h4 className="card-title title" style={{ textTransform: 'capitalize', fontWeight: 'bold' }}>Stores available:</h4>
									<hr></hr>
									{
										filteredStores && filteredStores
											.filter((store) => {
												return search.toLowerCase() === ''
													? store.name
													: store.name.toLowerCase().includes(search.toLowerCase());
											}).map((store) => {
												return (
													<>
														<ul className="list-group" style={{ listStyleType: 'none', textTransform: 'capitalize' }}>
															<li>
																<Link to={`/store/${store.id}`} state={{ store }} style={{ textDecoration: "none", color: "black" }}>
																	<div className={`card ${styles.card}`} style={{ padding: 5 }}>
																		<p className="card-text content">{store.name}</p>
																	</div>
																</Link>
															</li>
														</ul>
													</>
												)
											})
									}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<footer className={`text-center text-white ${styles.myFooter}`} style={{ backgroundColor: '#fafafa' }}>
				<div className="text-center text-dark p-3" style={{ backgroundColor: 'rgba(154, 152, 152, 0.2)' }}>
					© 2023 Copyright: GroceryFinds
				</div>
			</footer>

		</div>

	);
};

export default LocationComponent;
