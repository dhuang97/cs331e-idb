from sqlalchemy import text, inspect
from create_db import app, db, Item, Store, Location, item_stores, item_locations, store_locations, load_json
from sqlalchemy.exc import SQLAlchemyError


def modeldata(table, *relation_tables):
    instances_sql = text(f"SELECT * FROM {table}")
    instances_raw = db.session.execute(instances_sql)
    # list of instances and their attribute values
    instances_list = [list(instance) for instance in instances_raw]

    all_tables = ['item', 'store', 'location']
    other_tables = all_tables.copy()
    other_tables.remove(table)

    # add relational tables
    for i in range(len(instances_list)):
        instance = instances_list[i]
        id = instance[0]
        for other in other_tables:
            try:
                object_sql = text(
                    f"select {other}_id from {table}_{other}s where {table}_id = {id};")
                queryIds = list(db.session.execute(object_sql))
            except SQLAlchemyError:
                db.session.execute(text("ROLLBACK"))
                object_sql = text(
                    f"select {other}_id from {other}_{table}s where {table}_id = {id};")
                queryIds = list(db.session.execute(object_sql))
            object_names_list = []
            for object_id in queryIds:
                object_id = object_id[0]
                object_name_sql = text(
                    f"select name from {other} where id = {object_id};")
                object_name = list(db.session.execute(object_name_sql))[0][0]
                object_names_list.append(object_name)
            instances_list[i].append(object_names_list)

    instance_columns_sql = text(
        f"select * from information_schema.columns where table_name = '{table}';")
    instance_columns_raw = db.session.execute(instance_columns_sql)
    # list of column (attribute) names
    instance_columns_list = [list(key)[3] for key in instance_columns_raw]
    for relation_table in relation_tables:
        instance_columns_list.append(relation_table)

    result = [instances_list, instance_columns_list]
    return result
