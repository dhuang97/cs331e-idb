import React, { useState, useEffect } from 'react';
//import { Container, Row, Col } from 'react-bootstrap';
//import logoFull from '../assets/logoFull.png';
import styles from '../styles/LocationPages.module.css';
import { Link } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import InputGroup from 'react-bootstrap/InputGroup';
//import austin from '../assets/austin.png';
//import dallas from '../assets/dallas.png';
//import houston from '../assets/houston.png';
//import axios from 'axios';



const LocationPage = () => {
  // const URL = 'https://cs331e-379603.uc.r.appspot.com'
  let URL;
  if (window.location.hostname === 'localhost') {
    URL = 'http://127.0.0.1:5000/';
  } else {
    URL = 'https://cs331e-379603.uc.r.appspot.com';
  }

  const [data, setData] = useState(null);
  const [search, setSearch] = useState('');
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);



  // fetch data from backend

  useEffect(() => {
    fetch(`${URL}/locations`)
      .then(response => {
        if (response.ok) {
          return response.json()
        }
        throw response;
      })
      .then(data => {
        setData(data);
      })
      .catch(error => {
        console.error("Error fetching data: ", error)
        setError(error);
      })
      .finally(() => {
        setLoading(false);
      })

  }, [])

  if (loading) {
    return <div style={{ textAlign: 'center', fontSize: '20px' }}>Loading...</div>;
  }

  if (error) {
    return <div>Error fetching data...</div>;
  }

  // Sorting
  const handleSort = (colIndex, reverse) => {
    if (!data) return;

    const sortedData = [...data].sort((a, b) => {
      const colA = a[colIndex];
      const colB = b[colIndex];

      if (typeof colA === "string" && typeof colB === "string") {
        return colA.localeCompare(colB);
      }
      else {
        return colA < colB ? -1 : 1;
      }
    });
    if (reverse === 1) sortedData.reverse();
    setData(sortedData);
  };

  // Set data in list format to dict w/ keys
  const newData = data.map((item) => ({
    id: item[0],
    name: item[1],
    capital: item[2],
    img: item[3],
    population: item[4],
    region: item[5],
    items: item[6],
    stores: item[7]
  }));
  //useEffect(() => {axios.get('http://localhost:5000/locations').then(response => setLocations(response.data)).catch(error => console.log(error));}, []);

  console.log(newData);
  return (

    <div>
      {/* <!-- Page content --> */}
      <div id="page-container" className="w3-content" style={{ maxWidth: '2000px', marginTop: '46px' }}>
        <h2 className="w3-text-black" style={{ textAlign: 'center', textShadow: '1px 1px 0 #8f8c8c', fontSize: '60px' }}>Locations</h2>
        <center>
          <Button className="me-3" variant="outline-dark" onClick={() => handleSort(1, 0)}>Sort by name asc</Button>
          <Button className="me-3" variant="outline-dark" onClick={() => handleSort(1, 1)}>Sort by name desc</Button>
          <Button className="me-3" variant="outline-dark" onClick={() => handleSort(5, 0)}>Sort by region asc</Button>
          <Button className="me-3" variant="outline-dark" onClick={() => handleSort(5, 1)}>Sort by region desc</Button>
          <hr width="75%"></hr>
        </center>
        <br />

        <div className="container overflow-hidden text-center">
          <div>
            <Form>
              <InputGroup className='my-3'>

                {/* onChange for search */}
                <Form.Control
                  onChange={(e) => setSearch(e.target.value)}
                  placeholder='Search locations'
                />
              </InputGroup>
            </Form>
          </div>

          <div className={`${styles.row} mx-auto text-center`}>
            {newData
              .filter((data) => {
                return search.toLowerCase() === ''
                  ? data
                  : data.name.toLowerCase().includes(search.toLowerCase());
              }).map((location) => {

                return (
                  <>

                    <div className="col" style={{ padding: 20 }} key={location.id}>
                      <Link to={`/location/${location.id}`} state={{ location }} style={{ "textDecoration": "none", color: "black" }}>
                        <div className={`card ${styles.card}`} style={{ width: '23rem' }}>
                          <img src={location.img} className="card-img-top" style={{ height: '200px' }} alt={location.name} />
                          <div className="card-body">
                            <h5 className="card-title title" style={{ textTransform: 'capitalize', fontWeight: "bold" }}>{location.name}</h5>
                            <hr></hr>
                            <p className="card-text content">Capital: {location.capital} </p>
                            <p className="card-text content">Population: {location.population} </p>
                            <p className="card-text content">Region: {location.region} </p>
                          </div>
                        </div>
                      </Link>
                    </div>
                    <br />
                  </>
                )
              })
            }
          </div>
        </div>
      </div>

      <footer className={`text-center text-white ${styles.myFooter}`} style={{ backgroundColor: '#fafafa' }}>
        <div className="text-center text-dark p-3" style={{ backgroundColor: 'rgba(154, 152, 152, 0.2)' }}>
          © 2023 Copyright: GroceryFinds
        </div>
      </footer>
    </div>
  );
};

export default LocationPage;

