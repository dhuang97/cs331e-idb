import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import logoFull from '../assets/logoFull.png';
import styles from '../styles/Homepage.module.css';

const HomePage = () => {
  return (
    <div className={styles.pageContainer}>
      <div className={styles.backgroundImage} />
      <Container className="text-center py-5">
        <Row>
          <Col>
            <img src={logoFull} alt="Logo" className="img-fluid mb-4" style={{ maxWidth: '40%' }} />
            <div style={{paddingTop:'10%'}}>
            <h1>Grocery Finds</h1>
            <p className="lead">
              Introducing the ultimate shopping solution for savvy grocery shoppers - <br />
              a website that helps you find the best deals on your grocery list, every time!
            </p>
            </div>
          </Col>
        </Row>
      </Container>

      <footer className="text-center text-white" style={{ backgroundColor: '#fafafa' }}>
        <div className="text-center text-dark p-3" style={{ backgroundColor: 'rgba(154, 152, 152, 0.2)' }}>
          © 2023 Copyright: GroceryFinds
        </div>
      </footer>
    </div>
  );
};

export default HomePage;
