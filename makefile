.PHONY: backend frontend install run-backend run-frontend run clean
backend:
	@echo “Setting up backend environment...”
	python -m venv backend-env
	source backend/venv/Scripts/activate && pip install -r backend/requirements.txt
frontend:
	@echo “Setting up frontend environment...”
	cd frontend && npm install
install: backend frontend
run-backend:
	@echo “Starting backend server...”
	source backend-env/bin/activate && python backend/groceryfinds.py &
run-frontend:
	@echo “Starting frontend development server...”
	cd frontend && npm start
run: run-backend run-frontend
clean:
	@echo “Cleaning backend environment...”
	rm -rf backend-env
	@echo “Cleaning frontend environment...”
	cd frontend && rm -rf node_modules